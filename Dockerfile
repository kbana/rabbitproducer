FROM maven:3.5.2-jdk-8-alpine
ARG SPRING_ACTIVE_PROFILE
MAINTAINER kbana
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE && mvn package -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE

FROM openjdk:8-jdk-alpine
WORKDIR /usr/src/app
ADD config /usr/src/app/config
ADD target/merchantview-1.0.0.jar app.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=$SPRINT_ACTIVE_PROFILE", "-jar", "app.jar"]