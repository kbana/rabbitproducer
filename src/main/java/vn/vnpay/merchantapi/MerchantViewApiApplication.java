package vn.vnpay.merchantapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MerchantViewApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MerchantViewApiApplication.class, args);
    }

}
