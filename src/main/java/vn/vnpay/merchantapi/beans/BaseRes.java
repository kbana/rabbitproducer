package vn.vnpay.merchantapi.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.vnpay.merchantapi.constants.Rescode;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseRes<T> {

    @Schema(description = "Code response")
    private String code;

    @Schema(description = "Message response")
    private String message;

    @Schema(description = "Object response")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public static BaseRes of(Object data) {
        return of(Rescode.SUCCESS.code(), Rescode.SUCCESS.description(), data);
    }

    public static BaseRes of(Rescode resCode) {
        return of(resCode.code(), resCode.description());
    }

    public static BaseRes of(String code, String msg) {
        return of(code, msg, null);
    }

    public static BaseRes of(String code, String msg, Object data) {
        return BaseRes.builder().code(code).message(msg).data(data).build();
    }
}
