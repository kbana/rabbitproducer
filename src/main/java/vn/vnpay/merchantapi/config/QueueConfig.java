package vn.vnpay.merchantapi.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {

    @Bean
    Queue queue() {
        return new Queue("queue.merchantview.data", false);
    }
}
