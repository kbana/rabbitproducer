package vn.vnpay.merchantapi.constants;

public class Constant {
    public static final String MMS_TYPE = "MMS";
    public static final String CTT_TYPE = "CTT";
    public static final String POS_TYPE = "POS";
    public static final String HEADER_TYPE = "apiId";
}
