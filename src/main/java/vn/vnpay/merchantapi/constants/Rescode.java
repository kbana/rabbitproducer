/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.vnpay.merchantapi.constants;

/**
 *
 * @author khanhbn
 */
public enum Rescode {

    SUCCESS("00") {
        @Override
        public String description() {
            return "Success";
        }
    },
    FAILED("01") {
        @Override
        public String description() {
            return "Failed";
        }
    },
    REQUEST_NOT_FOUND("02") {
        @Override
        public String description() {
            return "Request not found";
        }
    },
    SEND_QUEUE_ERROR("03") {
        @Override
        public String description() {
            return "Send queue error";
        }
    },
    JSON_PROCESSING_ERROR("04") {
        @Override
        public String description() {
            return "Json processing exception";
        }
    },
    REQUEST_BODY_MISSING("05") {
        @Override
        public String description() {
            return "Required request body is missing";
        }
    };


    public final String i;

    Rescode(String i) {
        this.i = i;
    }

    public final String code() {
        return i;
    }

    public abstract String description();

}
