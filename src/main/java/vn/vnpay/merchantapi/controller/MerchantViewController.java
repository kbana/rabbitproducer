package vn.vnpay.merchantapi.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpay.merchantapi.beans.MerchantViewRequest;
import vn.vnpay.merchantapi.constants.Constant;
import vn.vnpay.merchantapi.exception.BusinessException;
import vn.vnpay.merchantapi.service.MerchantViewService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/merchant-view-api")
public class MerchantViewController {

    private MerchantViewService merchantViewService;

    @Autowired
    public MerchantViewController(MerchantViewService merchantViewService) {
        this.merchantViewService = merchantViewService;
    }

    @Operation(summary = "Api get request data from other system")
    @PostMapping(value = "/send")
    public ResponseEntity<?> search(@RequestHeader(value = "apiId", required = false) String apiId,
                                    @RequestBody String request) throws BusinessException,
            JsonProcessingException {
        return ResponseEntity.ok(merchantViewService.sendQueueMerchant(request, apiId));
    }
}
