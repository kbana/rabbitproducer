package vn.vnpay.merchantapi.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.vnpay.merchantapi.beans.BaseRes;
import vn.vnpay.merchantapi.beans.MerchantViewRequest;
import vn.vnpay.merchantapi.exception.BusinessException;

public interface MerchantViewService {

    /**
     * Gui thong tin merchant view len queue dong bo
     *
     * @param request
     * @return BaseRes
     * @throws BusinessException
     */
    BaseRes sendQueueMerchant(String request, String apiId) throws BusinessException, JsonProcessingException;
}
