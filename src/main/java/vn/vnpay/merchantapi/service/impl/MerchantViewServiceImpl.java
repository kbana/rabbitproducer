package vn.vnpay.merchantapi.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import vn.vnpay.merchantapi.beans.BaseRes;
import vn.vnpay.merchantapi.beans.MerchantViewRequest;
import vn.vnpay.merchantapi.constants.Rescode;
import vn.vnpay.merchantapi.exception.BusinessException;
import vn.vnpay.merchantapi.service.MerchantViewService;

@Service
@Slf4j
public class MerchantViewServiceImpl implements MerchantViewService {

    @Value("${queue.merchantView}")
    private String merchantViewApiQueue;

    private RabbitTemplate rabiTemplate;

    private ObjectMapper objectMapper;

    @Autowired
    public MerchantViewServiceImpl(RabbitTemplate rabiTemplate, ObjectMapper objectMapper) {
        this.rabiTemplate = rabiTemplate;
        this.objectMapper = objectMapper;
    }

    @Override
    public BaseRes sendQueueMerchant(String request, String apiId) throws BusinessException {
        log.info("Begin send queue merchant view with data: {} and queue: {}.", request, merchantViewApiQueue);
        if (Strings.isBlank(request) || Strings.isBlank(apiId)) {
            log.info("No data found with request: {} or apiId: {}.", request, apiId);
            throw new BusinessException(Rescode.REQUEST_NOT_FOUND);
        }
        try {
            MerchantViewRequest merchantViewRequest = MerchantViewRequest.builder()
                    .data(request)
                    .appId(apiId)
                    .build();
            String postData = objectMapper.writeValueAsString(merchantViewRequest);
            rabiTemplate.convertAndSend(merchantViewApiQueue, postData);
            log.info("End send queue merchant view queue name: {}.", merchantViewApiQueue);
        } catch (JsonProcessingException e) {
            log.error("Json processing exception: {}", e);
            throw new BusinessException(Rescode.JSON_PROCESSING_ERROR);
        } catch (Exception ex) {
            log.error("Send queue error {}:", ex);
            throw new BusinessException(Rescode.SEND_QUEUE_ERROR);
        }

        return BaseRes.of(Rescode.SUCCESS);
    }
}
